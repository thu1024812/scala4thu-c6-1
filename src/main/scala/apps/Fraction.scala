package apps

import cc.Fraction

/**
  * Created by mac023 on 2017/4/24.
  */
object FractionApp extends App{
  val frac=Fraction(1,0) plus Fraction(1,3)
  println(frac)
  val frac2=Fraction(1,2) equal Fraction(5,15)
  println(frac2)

}
