package apps

import cc.ListNode

/**
  * Created by mac023 on 2017/4/24.
  */
object ListNodeApp extends App{
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))
  println(listNode.size)
  println(listNode.filter(_!=3))
}
